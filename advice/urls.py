from django.urls import path
from . import views
from .views import terima_advice

app_name = 'advice'

urlpatterns = [
    path('', views.index_advice, name='index'),
    path('send/', terima_advice, name="ya"),
]