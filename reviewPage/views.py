from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from .forms import ReviewForm
from .models import ReviewModel

class ReviewView(TemplateView):
    template_name = 'reviewPage.html'


    def get(request, produk):
        temp = produk
        form = ReviewForm()
        reviews = ReviewModel.objects.filter(produknya=produk)
        args = {
            'reviews' : reviews,
            'produk' : produk
        }

        return render(request, 'reviewPage.html', args)

def review(request, produk):
    if request.method == 'POST':
        form = ReviewForm(request.POST)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.produknya = produk
            obj.save()
            return redirect('reviewPage:productReview', produk)
    else:
        form = ReviewForm()
    return render(request, 'reviewForm.html', {'form':form})

