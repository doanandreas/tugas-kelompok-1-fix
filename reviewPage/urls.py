from django.urls import path, re_path
from . import views

app_name = 'reviewPage'

urlpatterns = [
    path('<str:produk>/', views.ReviewView.get, name='productReview'),
    path('<str:produk>/add', views.review, name='addReview'),
]