from django.test import TestCase, Client
from homepage.models import Person
from homepage.forms import PersonForm
from homepage.views import home
from django.urls import reverse

class HomepageTest(TestCase):
    def test_apakah_ada_url_kosong_yang_menampilkan_homepage(self):
        c = Client()
        response = c.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('homepage/home.html')

    def test_apakah_terdapat_tulisan_welcome(self):
        c = Client()
        response = c.get('/')
        content = response.content.decode('utf-8')
        self.assertIn("Welcome", content)

    def test_apakah_terdapat_textbox_untuk_mengisi_nama(self):
        c = Client()
        response = c.get('/')
        content = response.content.decode('utf-8')
        self.assertIn("text", content)
        self.assertIn("name", content)

    def test_apakah_terdapat_selectbox_untuk_mengisi_gender(self):
        c = Client()
        response = c.get('/')
        content = response.content.decode('utf-8')
        self.assertIn("select", content)
        self.assertIn("gender", content)

    def test_apakah_terdapat_button_submit(self):
        c = Client()
        response = c.get('/')
        content = response.content.decode('utf-8')
        self.assertIn("<button", content)
        self.assertIn("submit", content)

    def membuat_object_Person(self , name = "nama_user", gender = "FEMALE"):
        return Person.objects.create(name = name, gender=gender)

    def test_apakah_ada_model_Person_dengan_field_nama_dan_gender(self):
        p = self.membuat_object_Person()
        semua_Person = Person.objects.all()
        self.assertTrue(isinstance(p, Person))
        self.assertEqual(semua_Person.count(), 1)

    def test_apakah_invalid_form_tidak_disimpan_dalam_database(self):
        p = Person(name = None, gender = None)
        data = {'name': p.name,
                'gender': p.gender}
        form = PersonForm(data = data)
        semua_Person = Person.objects.all()
        self.assertFalse(form.is_valid())
        self.assertEqual(semua_Person.count(), 0)
        self.assertTemplateUsed('homepage/home.html')

    def test_apakah_valid_form_disimpan_dalam_database(self):
        p = self.membuat_object_Person()
        data = {'name': p.name,
                'gender': p.gender}
        form = PersonForm(data = data)
        semua_Person = Person.objects.all()
        self.assertTrue(form.is_valid())
        self.assertEqual(semua_Person.count(), 1)
        self.assertTemplateUsed('products/hair.html')
       


class SummaryTest(TestCase):
    def test_apakah_ada_url_summary(self):
        c = Client()
        response = c.get('/summary/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('homepage/summary.html')

    def test_apakah_terdapat_tulisan_summary(self):
        c = Client()
        response = c.get('/summary/')
        content = response.content.decode('utf-8')
        self.assertIn("Summary", content)

    def test_apakah_terdapat_table(self):
        c = Client()
        response = c.get('/summary/')
        content = response.content.decode('utf-8')
        self.assertIn("<table", content)

    def test_apakah_terdapat_table_head(self):
        c = Client()
        response = c.get('/summary/')
        content = response.content.decode('utf-8')
        self.assertIn("<thead", content)

    def membuat_object_Person(self , name = "nama_user", gender = "FEMALE"):
        return Person.objects.create(name = name, gender=gender)

    def test_apakah_valid_form_ditampilkan_dalam_summary(self):
        p = self.membuat_object_Person()
        data = {'name': p.name,
                'gender': p.gender}
        form = PersonForm(data = data)

        c = Client()
        response = c.get('/summary/')
        content = response.content.decode('utf-8')
        self.assertIn("<td", content)
        self.assertIn(p.name, content)
        self.assertIn(p.gender, content)

        
    
