from django import forms
from .models import Person


class PersonForm(forms.ModelForm):
    class Meta:
        model = Person
        fields = ['name','gender']
        widgets = {
            'name' : forms.TextInput(attrs={
					'class': 'form-control'
					}),
            'gender' : forms.Select(attrs={
					'class': 'form-control'
					}),
        }