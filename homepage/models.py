from django.db import models
from products.models import hairCategory, faceCategory, skinCategory

gender = [
    ('MALE', 'MALE'),
    ('FEMALE', 'FEMALE'),
]

class Person(models.Model):
    name = models.CharField(max_length=50)
    gender = models.CharField(max_length=6, choices=gender)
    hair = models.ManyToManyField(hairCategory)
    face = models.ManyToManyField(faceCategory)
    skin = models.ManyToManyField(skinCategory)

    def __str__(self):
        return self.name

    def get_hair_values(self):
        hair = ''
        for h in hairCategory.objects.filter(nama = self.name).values_list('hairCategory', flat=True):
            hair = h
        return hair

    def get_skin_values(self):
        skin = ''
        for s in skinCategory.objects.filter(nama = self.name).values_list('skinCategory', flat=True):
            skin = s
        return skin

    def get_face_values(self):
        face = ''
        for f in faceCategory.objects.filter(nama = self.name).values_list('faceCategory', flat=True):
            face = f
        return face
    

    