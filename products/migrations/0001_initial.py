# Generated by Django 2.2.5 on 2019-10-19 13:53

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='faceCategory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama', models.CharField(max_length=20)),
                ('faceCategory', models.CharField(choices=[('BERMINYAK', 'BERMINYAK'), ('JERAWAT', 'JERAWAT'), ('KERING', 'KERING'), ('NORMAL', 'NORMAL')], max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='hairCategory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama', models.CharField(max_length=20)),
                ('hairCategory', models.CharField(choices=[('HAIR LOSS', 'HAIR LOSS'), ('DRY HAIR', 'DRY HAIR'), ('DANDRUFF', 'DANDRUFF')], max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='skinCategory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama', models.CharField(max_length=20)),
                ('skinCategory', models.CharField(choices=[('SENSITIF', 'SENSITIF'), ('KERING', 'KERING'), ('NORMAL', 'NORMAL')], max_length=20)),
            ],
        ),
    ]
