from django.contrib import admin
from products.models import hairCategory, faceCategory, skinCategory

# Register your models here.
admin.site.register(hairCategory)
admin.site.register(faceCategory)
admin.site.register(skinCategory)
