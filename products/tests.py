from django.test import TestCase, Client
from products.models import hairCategory, faceCategory, skinCategory
from products.forms import hairCategoryForm, faceCategoryForm, skinCategoryForm

# Create your tests here.
class HairCategoryPage(TestCase):
    def test_apakah_ada_url_yang_menampilkan_products_hair(self):
        c = Client()
        response = c.get('/products/hair/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('products/hair.html')

    def test_apakah_ada_tulisan_hello(self):
        c = Client()
        response = c.get('/products/hair/')
        content = response.content.decode('utf-8')
        self.assertIn('Hello', content)

    def test_apakah_terdapat_textbox_untuk_mengisi_nama(self):
        c = Client()
        response = c.get('/products/hair/')
        content = response.content.decode('utf-8')
        self.assertIn("nama", content)

    def test_apakah_terdapat_selectbox_untuk_memilih_kategori_rambut(self):
        c = Client()
        response = c.get('/products/hair/')
        content = response.content.decode('utf-8')
        self.assertIn("hairCategory", content)

    def test_apakah_terdapat_button_submit(self):
        c = Client()
        response = c.get('/products/hair/')
        content = response.content.decode('utf-8')
        self.assertIn("<button", content)
        self.assertIn("submit", content)

class FaceCategoryPage(TestCase):
    def test_apakah_ada_url_yang_menampilkan_products_face(self):
        c = Client()
        response = c.get('/products/face/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('products/face.html')

    def test_apakah_ada_tulisan_hello(self):
        c = Client()
        response = c.get('/products/face/')
        content = response.content.decode('utf-8')
        self.assertIn('Hello', content)

    def test_apakah_terdapat_textbox_untuk_mengisi_nama(self):
        c = Client()
        response = c.get('/products/face/')
        content = response.content.decode('utf-8')
        self.assertIn("nama", content)

    def test_apakah_terdapat_selectbox_untuk_memilih_kategori_faceskin(self):
        c = Client()
        response = c.get('/products/face/')
        content = response.content.decode('utf-8')
        self.assertIn("faceCategory", content)

    def test_apakah_terdapat_button_submit(self):
        c = Client()
        response = c.get('/products/face/')
        content = response.content.decode('utf-8')
        self.assertIn("<button", content)
        self.assertIn("submit", content)

class SkinCategoryPage(TestCase):
    def test_apakah_ada_url_yang_menampilkan_products_skin(self):
        c = Client()
        response = c.get('/products/skin/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('products/skin.html')

    def test_apakah_ada_tulisan_hello(self):
        c = Client()
        response = c.get('/products/skin/')
        content = response.content.decode('utf-8')
        self.assertIn('Hello', content)

    def test_apakah_terdapat_textbox_untuk_mengisi_nama(self):
        c = Client()
        response = c.get('/products/skin/')
        content = response.content.decode('utf-8')
        self.assertIn("nama", content)

    def test_apakah_terdapat_selectbox_untuk_memilih_kategori_skin(self):
        c = Client()
        response = c.get('/products/skin/')
        content = response.content.decode('utf-8')
        self.assertIn("skinCategory", content)

    def test_apakah_terdapat_button_submit(self):
        c = Client()
        response = c.get('/products/skin/')
        content = response.content.decode('utf-8')
        self.assertIn("<button", content)
        self.assertIn("submit", content)
