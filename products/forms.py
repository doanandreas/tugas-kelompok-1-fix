from django import forms
from .models import hairCategory, faceCategory, skinCategory

class hairCategoryForm(forms.ModelForm):
    class Meta:
        model = hairCategory
        fields = {'nama','hairCategory'}
        widgets = {'nama' : forms.TextInput(
            attrs = {
                'class':'form-control',
            }
        ),
                'hairCategory': forms.Select(
                    attrs = {
                        'class': 'form-control',
                    }
                )}

class faceCategoryForm(forms.ModelForm):
    class Meta:
        model = faceCategory
        fields = {'nama','faceCategory'}
        widgets = {'nama' : forms.TextInput(
            attrs = {
                'class':'form-control',
            }
        ),
            'faceCategory': forms.Select(
                    attrs = {
                        'class': 'form-control',
                    }
                )}

class skinCategoryForm(forms.ModelForm):
    class Meta:
        model = skinCategory
        fields = {'nama','skinCategory'}
        widgets = {'nama' : forms.TextInput(
            attrs = {
                'class':'form-control',
            }
        ),
        'skinCategory': forms.Select(
                    attrs = {
                        'class': 'form-control',
                    }
                )}