from django.shortcuts import render,redirect
from django.http import HttpResponse
from homepage.models import Person
from .models import hairCategory, faceCategory, skinCategory
from .forms import hairCategoryForm, faceCategoryForm, skinCategoryForm

# Create your views here.
def hair(request):
    hair = hairCategoryForm(request.POST or None)
    if request.method == 'POST':
        if hair.is_valid():
            nama = request.POST['nama']
            if Person.objects.filter(name = nama).exists():
                obj = hair.save()
                if obj.hairCategory == 'DANDRUFF':
                    return redirect('products:dandruff')
                elif obj.hairCategory == 'HAIR LOSS':
                    return redirect('products:hairLoss')
                elif obj.hairCategory == 'DRY HAIR':
                    return redirect('products:dryHair')
            else:
                argument = {
                    'hairCategory': hair,
                    'gagal' : True
                }
                return render(request, 'hair.html', argument)
    
    argument = {
        'hairCategory': hair,
        'gagal' : False
    }
    return render(request, 'hair.html', argument)

def dandruff(request):
    return render(request,'dandruff.html')

def hairLoss(request):
    return render(request,'hairLoss.html')

def dryHair(request):
    return render(request,'dryHair.html')

def face(request):
    face = faceCategoryForm(request.POST or None)
    if request.method == 'POST':
        if face.is_valid():
            nama = request.POST['nama']
            if Person.objects.filter(name = nama).exists():
                obj = face.save()
                if obj.faceCategory == 'JERAWAT':
                    return redirect('products:jerawat')
                elif obj.faceCategory == 'BERMINYAK':
                    return redirect('products:berminyak')
                elif obj.faceCategory == 'KERING':
                    return redirect('products:keringFace')
                elif obj.faceCategory == 'NORMAL':
                    return redirect('products:normalFace')
            else:
                argument = {
                    'faceCategory': face,
                    'gagal' : True
                }
            return render(request, 'face.html', argument)

    argument = {
        'faceCategory': face,
        'gagal' : False
    }
    return render(request, 'face.html', argument)

def jerawat(request):
    return render(request,'jerawat.html')

def berminyak(request):
    return render(request,'berminyak.html')

def keringFace(request):
    return render(request,'keringFace.html')

def normalFace(request):
    return render(request,'normalFace.html')

def skin(request):
    skin = skinCategoryForm(request.POST or None)
    if request.method == 'POST':
        if skin.is_valid():
            nama = request.POST['nama']
            if Person.objects.filter(name = nama).exists():
                obj = skin.save()
                if obj.skinCategory == 'NORMAL':
                    return redirect('products:normalSkin')
                elif obj.skinCategory == 'KERING':
                    return redirect('products:drySkin')
                elif obj.skinCategory == 'SENSITIF':
                    return redirect('products:sensitifSkin')
                return redirect('products:skin')
            else:
                argument = {
                    'skinCategory': face,
                    'gagal' : True
                }
            return render(request, 'skin.html', argument)

    argument = {
        'skinCategory': skin,
        'gagal': False
    }
    return render(request, 'skin.html', argument)

def normalSkin(request):
    return render(request,'normalSkin.html')

def drySkin(request):
    return render(request,'drySkin.html')

def sensitifSkin(request):
    return render(request,'sensitifSkin.html')