from django.urls import path
from . import views

app_name = 'products'

urlpatterns = [
    path('hair/', views.hair, name='hair'),
    path('dandruff/', views.dandruff, name='dandruff'),
    path('hairLoss/', views.hairLoss, name='hairLoss'),
    path('dryHair/', views.dryHair, name='dryHair'),
    path('face/', views.face, name='face'),
    path('jerawat/', views.jerawat, name='jerawat'),
    path('berminyak/', views.berminyak, name='berminyak'),
    path('keringFace/', views.keringFace, name='keringFace'),
    path('normalFace/', views.normalFace, name='normalFace'),
    path('skin/', views.skin, name='skin'),
    path('normalSkin/', views.normalSkin, name='normalSkin'),
    path('drySkin/', views.drySkin, name='drySkin'),
    path('sensitifSkin/', views.sensitifSkin, name='sensitifSkin'),
]